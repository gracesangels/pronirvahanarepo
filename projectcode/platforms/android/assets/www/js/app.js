// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ionic-material', 'ionMdInput', 'firebase'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.factory("Auth", ["$firebaseAuth",
  function($firebaseAuth) {
    var ref = new Firebase("https://pronirvahanadb.firebaseio.com/");
    return $firebaseAuth(ref);
  }
])

.factory("UserDataService", ['$rootScope', function($rootScope){
	var userName = "undefined";
	var userOrg = "undefined";
	var userMail = "undefined";
	
	var factory = {};
	
	factory.getUserName = function(){
	console.log(userName + ":" + userOrg + ":" + userMail);
					return userName;
	}
	
	factory.getUserOrg = function(){
	console.log(userName + ":" + userOrg + ":" + userMail);
					return userOrg;
	}
	
	factory.getUserMail = function(){
	console.log(userName + ":" + userOrg + ":" + userMail);
					return userMail;
	}
	
	factory.setUserData = function(){
		dbRef = new Firebase("https://pronirvahanadb.firebaseio.com/");
		authData = dbRef.getAuth();
		var childRef = dbRef.child('users/' + authData.uid);
	
		console.log("The DB path to the current user: " + childRef.toString());
		
		childRef.once("value", function(snapshot)
		{
			if(snapshot.exists())
			{
				console.log("Retrieved the details of the user: " + authData.uid);
				var data = snapshot.val();
				userName = data.userName;
				console.log("The user name is : " + userName);
				userOrg = data.userOrgCode;
				console.log("The user org is : " + userOrg);
				userMail = data.userMail;
				console.log("The user mail is : " + userMail);
				$rootScope.$broadcast('userDataDone');
			}
		});
	}
	
	return factory;
}])

.factory("LoginService", ['$location', '$timeout', '$rootScope', function($location, $timeout, $rootScope)
{
	return{
		login : function(userCred)
		{
			var ref = new Firebase("https://pronirvahanadb.firebaseio.com/");
			console.log("Received the login details. Attempting to login.");
			console.log("email id is: " + userCred.mail);
			console.log("password is: " + userCred.password);
			ref.authWithPassword
			(
				{
					email	 : userCred.mail,
					password : userCred.password
				}, 
				function(error, user) 
				{
					if (error) 
					{
						console.log("Invalid Credentials.");
						$rootScope.$broadcast('loginFail');
					}
					else 
					{
						console.log("Logged in as:", user.uid);
						$rootScope.$broadcast('loginSuccess');
					}
				}
			);
		}
	}
}])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    // Turn off caching for demo simplicity's sake
    $ionicConfigProvider.views.maxCache(0);

    /*
    // Turn off back button text
    $ionicConfigProvider.backButton.previousTitleText(false);
    */

    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })
	
	.state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            },
            'fabContent': {
                template: ''
            }
        }
    })
	
	.state('app.register', {
        url: '/register',
        views: {
            'menuContent': {
                templateUrl: 'templates/register.html',
                controller: 'RegisterCtrl'
            },
            'fabContent': {
                template: ''
            }
        }
    })

	.state('app.signUp', {
        url: '/signUp',
        views: {
            'menuContent': {
                templateUrl: 'templates/signUp.html',
                controller: 'SignUpCtrl'
            },
            'fabContent': {
                template: ''
            }
        }
    })
	
    .state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl',
				resolve:
				{
					// controller will not be loaded until $requireAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					"currentAuth": ["Auth", function(Auth)
					{
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError
						return Auth.$requireAuth();
					}]
				}
            },
            'fabContent': {
                template: '<button id="fab-home" class="button button-fab button-fab-top-right expanded button-energized-900 flap"><i class="icon ion-paper-airplane"></i></button>',
                controller: function ($timeout) {
                   /* $timeout(function () {
                        document.getElementById('fab-home').classList.toggle('on');
                    }, 200); */
                }
            }
        }
    })

    .state('app.projects', {
        url: '/projects',
        views: {
            'menuContent': {
                templateUrl: 'templates/projects.html',
                controller: 'ProjectsCtrl'
            },
            'fabContent': {
                template: '<button id="fab-projects" class="button button-fab button-fab-top-left expanded button-energized-900 spin"><i class="icon ion-chatbubbles"></i></button>',
                controller: function ($timeout) {
                   /* $timeout(function () {
                        document.getElementById('fab-projects').classList.toggle('on');
                    }, 900); */
                }
            }
        }
    })
	
    .state('app.profile', {
        url: '/profile',
        views: {
            'menuContent': {
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl'
            },
            'fabContent': {
                template: '<button id="fab-profile" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>',
                controller: function ($timeout) {
                    /*$timeout(function () {
                        document.getElementById('fab-profile').classList.toggle('on');
                    }, 800);*/
                }
            }
        }
    })
    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
});
