// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'starter.controllers', 'ionic-material', 'ionMdInput', 'firebase'])

app.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
});

app.factory("dbURL", [function()
{
	var dbURL = "https://pronirvahanadb.firebaseio.com/";
	return dbURL;
}]);

app.factory("Auth", ["dbURL", "$firebaseAuth",
  function(dbURL, $firebaseAuth) {
    var ref = new Firebase(dbURL);
    return $firebaseAuth(ref);
  }
]);

app.factory("UserDataService", ['$rootScope', 'dbURL', function($rootScope, dbURL){
	var userName = "undefined";
	var userOrg = "undefined";
	var userMail = "undefined";
	
	var factory = {};
	
	factory.getUserName = function(){
	console.log(userName + ":" + userOrg + ":" + userMail);
					return userName;
	}
	
	factory.getUserOrg = function(){
	console.log(userName + ":" + userOrg + ":" + userMail);
					return userOrg;
	}
	
	factory.getUserMail = function(){
	console.log(userName + ":" + userOrg + ":" + userMail);
					return userMail;
	}
	
	factory.setUserData = function(){
		dbRef = new Firebase(dbURL);
		authData = dbRef.getAuth();
		var childRef = dbRef.child('users/' + authData.uid);
	
		console.log("The DB path to the current user: " + childRef.toString());
		
		childRef.once("value", function(snapshot)
		{
			if(snapshot.exists())
			{
				console.log("Retrieved the details of the user: " + authData.uid);
				var data = snapshot.val();
				userName = data.userName;
				console.log("The user name is : " + userName);
				userOrg = data.userOrgCode;
				console.log("The user org is : " + userOrg);
				userMail = data.userMail;
				console.log("The user mail is : " + userMail);
				$rootScope.$broadcast('userDataDone');
			}
		});
	}
	
	return factory;
}]);

app.factory("LoginService", ['$location', '$timeout', '$rootScope', 'dbURL', function($location, $timeout, $rootScope, dbURL)
{
	return{
		login : function(userCred)
		{
			var ref = new Firebase(dbURL);
			console.log("Received the login details. Attempting to login.");
			console.log("email id is: " + userCred.mail);
			console.log("password is: " + userCred.password);
			ref.authWithPassword
			(
				{
					email	 : userCred.mail,
					password : userCred.password
				}, 
				function(error, user) 
				{
					if (error) 
					{
						console.log("Invalid Credentials.");
						$rootScope.$broadcast('loginFail');
					}
					else 
					{
						console.log("Logged in as:", user.uid);
						$rootScope.$broadcast('loginSuccess');
					}
				}
			);
		}
	}
}]);

app.factory("LocationService",["dbURL", "UserDataService", "$q", function(dbURL, UserDataService, $q)
{
	var factory = {};
		
	factory.getLocationList = function()
	{
		var deferObj = $q.defer();
		var orgid = UserDataService.getUserOrg();
		var locationList = [];
		var locRef = new Firebase(dbURL + 'locations/' + orgid);
		locRef.once("value", function(snapshot) {
			snapshot.forEach(function(childSnapshot) {
				var locId = childSnapshot.key();
				var locData = childSnapshot.val();
				locData['id'] = locId;
				locationList.push(locData);		
				console.log("SELECTED -> " + locData.districtName + ":" + locData.stateName + ":" + locData.id);
			});
			deferObj.resolve(locationList);
		});
		return deferObj.promise;
	}
	
	
	factory.addLocation = function(locObj)
	{
		var deferObj = $q.defer();
		var orgid = UserDataService.getUserOrg();
		var locRef = new Firebase(dbURL + 'locations/' + orgid + "/" + locObj.locationId);
		console.log("The path to location: "  + locRef.toString());
		locRef.once("value", function(snapshot) {
			if(!snapshot.exists())
			{
				locRef.set({districtName:locObj.district, stateName:locObj.state},function(error)
				{
					if(error)
					{
						console.log("The location could not be added");
						deferObj.resolve(0);
					}
					else
					{
						console.log("The location added");
						
						var insRef = new Firebase(dbURL + "instances/" + orgid + "/" + locObj.locationId + "/" + locObj.instanceId);
						insRef.set({address: locObj.address}, function(error)
						{
							if(error)
							{
								console.log("The location is added; but the instance data could not be added");
								deferObj.resolve(1);
							}
							else
							{
								console.log("Instance data added");
								deferObj.resolve(2);
							}
						});
					}
				});
			}
			else
			{
				console.log("The location is already present. Good to go");
				var insRef = new Firebase(dbURL + "instances/" + orgid + "/" + locObj.locationId + "/" + locObj.instanceId);
				insRef.once("value", function(insnapshot) {
					if(insnapshot.exists())
					{
						var insdata = insnapshot.val();
						console.log("The location and address already exist. Address found is : " + insdata.address);
						deferObj.resolve(3);
					}
					else
					{
						insRef.set({address: locObj.address},
						function(error)
						{
							if(error)
							{
								console.log("The instance data could not be added");
								deferObj.resolve(1);
							}
							else
							{
								console.log("Instance data added");
								deferObj.resolve(2);
							}
						});
					}
				});
			}
		});
		return deferObj.promise;
	}
		
	return factory;
}]);

app.factory("ProjectService", ["dbURL", "UserDataService", "$rootScope", "LocationService", "$q", "PeopleService", function(dbURL, UserDataService, $rootScope, LocationService, $q, PeopleService)
{
	var factory = {};
		
	factory.getProjectList = function()
	{
		var deferObj = $q.defer();
		var orgid = UserDataService.getUserOrg();
		var projectList = [];	
		var projRef = new Firebase(dbURL + 'projects/' + orgid);
		projRef.once("value", function(snapshot) {
			snapshot.forEach(function(childSnapshot) {
				var projId = childSnapshot.key();
				var projData = childSnapshot.val();
				projData['id'] = projId;
				projectList.push(projData);		
				console.log("SELECTED -> " + projData.projectName + ":" + projData.projectManager + ":" + projData.projectDescription + ":" + projData.startDate + ":" + projData.endDate + ":" + projData.duration + ":" + projData.id);
			});
			deferObj.resolve(projectList);
		});
		return deferObj.promise;
	}

	factory.addProject = function(projData)
	{
		var deferObj = $q.defer();
		var orgid = UserDataService.getUserOrg();
		
		var ref = new Firebase(dbURL + "projects/" + orgid + "/" + projData.id);
		console.log("project ref : " + ref.toString());
		ref.once("value", function(snapshot) {
			if(snapshot.exists())
			{
				var projdata = snapshot.val();
				console.log("The name of the project : " + projdata.projectName);
				deferObj.resolve(0);
			}
			else
			{
				PeopleService.getPerson(orgid, projData.mngr).then(function(perId)
				{
					if(perId)
					{
						ref.set({projectName : projData.name, projectDescription : projData.desc, projectManager : projData.mngr, endDate : projData.end, startDate : projData.start, duration: projData.time},
						function(error)
						{
							if(error)
							{
								console.log("The project data could not be added");
								deferObj.resolve(1);
							}
							else
							{
								console.log("Project data added");
								deferObj.resolve(2);
							}
						});
					}
					else
					{
						console.log("The manager is not registered");
						deferObj.resolve(3);
					}
				});
			}
		});
		return deferObj.promise;
	}
	
	factory.addFunds = function(fund)
	{
		var deferObj = $q.defer();
		var orgid = UserDataService.getUserOrg();
		var projref = new Firebase(dbURL + "projectFinance/" + orgid + "/" + fund.prid);
		var locref = new Firebase(dbURL + "projectFinance/" + orgid + "/" + fund.prid + "/projectLocations/" + fund.lid);
		var insref = new Firebase(dbURL + "projectFinance/" + orgid + "/" + fund.prid + "/projectLocations/" + fund.lid + "/projectInstances/" + fund.iid);
		
		insref.update({instanceFunds : fund.amt}, function(error)
		{
			if(error)
			{
				console.log("Could not add funds for instance");
				deferObj.resolve(0);
			}
			else
			{
				console.log("Added funds for instance");
				locref.once("value", function(snapshot)
				{
					var locFund = fund.amt;
					if(snapshot.hasChild('locationFunds'))
					{
						var data = snapshot.val();
						locFund += data.locationFunds;
					}
					locref.update({locationFunds : locFund}, function(error)
					{
						if(error)
						{
							console.log("Could not add funds for location");
							deferObj.resolve(1);
						}
						else
						{
							console.log("Added funds for locations");
							projref.once("value", function(childSnapshot)
							{
								
								var projFund = fund.amt;
								if(childSnapshot.hasChild('projectFunds'))
								{
									var data = childSnapshot.val();
									projFund += data.projectFunds;
								}
								projref.update({projectFunds : projFund}, function(error)
								{
									if(error)
									{
										console.log("Could not add funds for project");
										deferObj.resolve(2);
									}
									else
									{
										console.log("added the funds for project");
										deferObj.resolve(3);
									}
								});
							});
						}
					});
				});			
				
			}
		});
		return deferObj.promise;
	}
	
	factory.addExpenses = function(expense)
	{
		var deferObj = $q.defer();
		var orgid = UserDataService.getUserOrg();
		var projref = new Firebase(dbURL + "projectFinance/" + orgid + "/" + expense.prid);
		var locref = new Firebase(dbURL + "projectFinance/" + orgid + "/" + expense.prid + "/projectLocations/" + expense.lid);
		var insref = new Firebase(dbURL + "projectFinance/" + orgid + "/" + expense.prid + "/projectLocations/" + expense.lid + "/projectInstances/" + expense.iid);
		
		insref.update({instanceExpense : expense.amt}, function(error)
		{
			if(error)
			{
				console.log("Could not add expenses for instance");
				deferObj.resolve(0);
			}
			else
			{
				console.log("Added expense for instance");
				locref.once("value", function(snapshot)
				{
					var locExp = expense.amt;
					if(snapshot.hasChild('locationExpense'))
					{
						var data = snapshot.val();
						locExp += data.locationExpense;
					}
					locref.update({locationExpense : locExp}, function(error)
					{
						if(error)
						{
							console.log("Could not add expense for location");
							deferObj.resolve(1);
						}
						else
						{
							console.log("Added expense for locations");
							projref.once("value", function(childSnapshot)
							{
								
								var projExp = expense.amt;
								if(childSnapshot.hasChild('projectExpense'))
								{
									var data = childSnapshot.val();
									projExp += data.projectExpense;
								}
								projref.update({projectExpense : projExp}, function(error)
								{
									if(error)
									{
										console.log("Could not add expense for project");
										deferObj.resolve(2);
									}
									else
									{
										console.log("added the expense for project");
										deferObj.resolve(3);
									}
								});
							});
						}
					});
				});			
				
			}
		});
		return deferObj.promise;
	}
	
	return factory;
}]);

app.factory("InstanceService",["dbURL", "UserDataService", "$q", function(dbURL, UserDataService, $q)
{
	var factory = {};

	factory.getInstanceList = function(locId)
	{
		var deferObj = $q.defer();
		var instanceList = [];
		var orgid = UserDataService.getUserOrg();
		var insRef = new Firebase(dbURL + 'instances/' + orgid + "/" + locId);
		insRef.once("value", function(snapshot) 
		{
			console.log("Selecting instances for location: " + locId);
			snapshot.forEach(function(childSnapshot) 
			{
				var insId = childSnapshot.key();
				var insData = childSnapshot.val();
				insData['id'] = insId;
				instanceList.push(insData);		
				console.log("SELECTED -> " + insData.address + ":" + insData.id);
			});
			deferObj.resolve(instanceList);
		});
		return deferObj.promise;
	}
	return factory;
}]);

app.factory("PeopleService",["dbURL", "UserDataService", "$q", function(dbURL, UserDataService, $q)
{
	var factory = {};

	factory.getPeopleList = function()
	{
		var deferObj = $q.defer();
		var peopleList = [];
		var orgid = UserDataService.getUserOrg();
		var pplRef = new Firebase(dbURL + 'personnel/' + orgid);
		pplRef.on("value", function(snapshot)
		{
			snapshot.forEach(function(childSnapshot)
			{
				var pplid = childSnapshot.key();
				var pplData = childSnapshot.val();
				pplData['id'] = pplid;
				peopleList.push(pplData);		
				console.log("SELECTED -> " + pplData.employeeName + ":" + pplData.employeeRole + ":" + pplData.employeeMail + ":" + pplData.gender + ":" + pplData.number + ":" + pplData.id);
			});
			deferObj.resolve(peopleList);
		});
		return deferObj.promise;
	}
		
	return factory;
}]);

app.factory("ItemService",["dbURL", "UserDataService", "$q", function(dbURL, UserDataService, $q)
{
	var factory = {};

	factory.getItemList = function()
	{
		var deferObj = $q.defer();
		var itemList = [];
		var orgid = UserDataService.getUserOrg();
		var itemRef = new Firebase(dbURL + 'inventory/' + orgid);
		itemRef.on("value", function(snapshot)
		{
			snapshot.forEach(function(childSnapshot)
			{
				var itemid = childSnapshot.key();
				var itemData = childSnapshot.val();
				itemData['id'] = itemid;
				itemList.push(itemData);		
				console.log("SELECTED -> " + itemData.itemName + ":" + itemData.itemQuantity + ":" + itemData.id);
			});
			deferObj.resolve(itemList);
		});
		return deferObj.promise;
	}
	return factory;
}]);

app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    // Turn off caching for demo simplicity's sake
    $ionicConfigProvider.views.maxCache(0);

    /*
    // Turn off back button text
    $ionicConfigProvider.backButton.previousTitleText(false);
    */

    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    });
	
	$stateProvider.state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            },
            'fabContent': {
                template: ''
            }
        }
    })
	
	$stateProvider.state('app.register', {
        url: '/register',
        views: {
            'menuContent': {
                templateUrl: 'templates/register.html',
                controller: 'RegisterCtrl'
            },
            'fabContent': {
                template: ''
            }
        }
    })

	$stateProvider.state('app.signUp', {
        url: '/signUp',
        views: {
            'menuContent': {
                templateUrl: 'templates/signUp.html',
                controller: 'SignUpCtrl'
            },
            'fabContent': {
                template: ''
            }
        }
    })
	
    $stateProvider.state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl',
				resolve:
				{
					// controller will not be loaded until $requireAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					"currentAuth": ["Auth", function(Auth)
					{
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError
						return Auth.$requireAuth();
					}]
				}
            },
            'fabContent': {
                template: ''
            }
        }
    })

    $stateProvider.state('app.projects', {
        url: '/projects',
        views: {
            'menuContent': {
                templateUrl: 'templates/projects.html',
                controller: 'ProjectsCtrl'
            },
            'fabContent': {
                template: '<button id="fab-projects" class="button button-fab button-fab-top-left expanded button-energized-900 spin"><i class="icon ion-chatbubbles"></i></button>',
                controller: function ($timeout) {
                   /* $timeout(function () {
                        document.getElementById('fab-projects').classList.toggle('on');
                    }, 900); */
                }
            }
        }
    })
	
    $stateProvider.state('app.profile', {
        url: '/profile',
        views: {
            'menuContent': {
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl'
            },
            'fabContent': {
                template: '<button id="fab-profile" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>',
                controller: function ($timeout) {
                    /*$timeout(function () {
                        document.getElementById('fab-profile').classList.toggle('on');
                    }, 800);*/
                }
            }
        }
    })
    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
});
