/* global angular, document, window */
'use strict';

angular.module('starter.controllers', [])

.controller('HomeCtrl', function($scope, $location, UserDataService)
{
	$scope.userName = UserDataService.getUserName();
	$scope.dbRef = new Firebase("https://pronirvahanadb.firebaseio.com/");	
	
	$scope.logOut = function()
	{
		var authData = $scope.dbRef.getAuth();
		if(authData)
		{
			console.log("Logging out the user " + authData.uid);
			$scope.dbRef.unauth();
			$location.path('/login');
		}
		else
		{
			console.log("The user is already logged out");
			$location.path('/login');
		}
	}
})

.controller('LoginCtrl', function($scope, $location, $firebaseAuth, UserDataService, $timeout)
{	
	$scope.ref = new Firebase("https://pronirvahanadb.firebaseio.com/");
		
	$scope.loginUser = function()
	{
		if($scope.userMail && $scope.userPassword)
		{
			console.log("Received the login details. Attempting to login.");
			console.log("email id is: " + $scope.userMail);
			console.log("password is: " + $scope.userPassword);
			$scope.ref.authWithPassword
			(
				{
					email : $scope.userMail,
					password : $scope.userPassword
				}, 
				function(error, userData) 
				{
					if (error) 
					{
						$scope.userMail = "";
						$scope.userPassword = "";
						console.log("Invalid Credentials.");
						alert("Invalid Credentials.");
					}
					else 
					{
						console.log("Logged in as:", userData.uid);
						UserDataService.setUserData();
						$timeout( function(){ $location.path('/home'); $scope.$apply();}, 300);	
					}
				}
			);
		}
	}
	
	$scope.forgotPassword = function()
	{
		if($scope.userMail)
		{
			console.log("Received the email id.");
			$scope.ref.resetPassword(
			{
				email: $scope.userMail
			}, 
			function(error)
			{
				if (error)
				{
					switch (error.code)
					{
						case "INVALID_USER":
							console.log("The specified user account does not exist.");
							alert("The specified user account does not exist. Do provide a valid email");
							break;
						default:
						console.log("Error resetting password:", error);
						alert("The password could not be reset. Please try again later.");
					}
				}
				else
				{
					console.log("Password reset email sent successfully!");
					alert("Password reset email sent successfully!");
				}
			});
		}
	}
})

.controller('SignUpCtrl', function($scope, $firebaseObject, $location)
{		
	console.log("Waiting for User details to register.");
	
	var addUserData = function(authId)
	{
		var userDbRef = new Firebase("https://pronirvahanadb.firebaseio.com/");
		var userPath = userDbRef.child("users/" + authId);
		userPath.set({userName: $scope.userName, userMail: $scope.userMail, userOrgCode: $scope.userOrgCode},
		function(error)
		{
			if(error)
			{
				console.log("Error adding user data");
				userDbRef.removeUser({
										email: $scope.userMail,
										password: $scope.userPassword
									});
				alert("Registration unsuccessful. Do try after sometime");
			}
			else
			{
				console.log("User details added");
				alert("Registration successful.");
			}
		});
	}
	
	$scope.registerUser = function()
	{
	    if ($scope.userName && $scope.userMail && $scope.userPassword && $scope.userOrgCode)
		{
			console.log("Received the user details. Verifying if the organisation code is valid.");
			var userDbRef = new Firebase("https://pronirvahanadb.firebaseio.com/");
	        userDbRef.once("value", function(snapshot) 
			{
				if(snapshot.hasChild("organisations/" + $scope.userOrgCode))
				{
					console.log("The organisation is registered. Attempting to register the user.");
					userDbRef.createUser
					(
						{
							email    : $scope.userMail,
							password : $scope.userPassword
						},
						function(error, userData) 
						{
							if (error) 
							{
								switch (error.code)
								{
									case "EMAIL_TAKEN":
										console.log("The new user account cannot be created because the email is already in use." + $scope.userMail);
										var alertMsg = "The new user account cannot be created because the email is already in use.";
										alertMsg += "In case, you have forgotten password, request for a password reset.";
										alert(alertMsg);
										break;
									case "INVALID_EMAIL":
										console.log("The specified email is not valid : " + $scope.userMail);
										alert("The specified email is not valid.");
										break;
									default:
										console.log("Error creating user:", error);
										alert("Registration unsuccessful. Do try after sometime.");
								}
							} 
							else 
							{
								addUserData(userData.uid);
							}
						}
					);
				}
				else
				{
				    console.log("Invalid Organisation Code: " + $scope.userOrgCode);
					alert("Invalid Organisation Code. Do register your organisation before signing up.");
				}
			});
			$location.path("/login");
		}
	}
	
	$scope.cancelUserRegister = function()
	{
		console.log("User Registration action is cancelled by the user");
	    $scope.userName = "";
		$scope.userMail = "";
		$scope.userPassword = "";
		$scope.userOrgCode = "";
		$location.path("/login");
	}
})

.controller('RegisterCtrl', function($scope, $firebaseObject, $location)
{
	$scope.orgName = "";
	$scope.orgMail = "";
	$scope.code = "";
	$scope.dbRef = "";
    var found = false;
	
	console.log("Waiting for Organisation details to register.");

	$scope.addOrganisationData = function()
	{
	    console.log("It is a new Organisation. Hence, attempting to save the organisation details.");
				
		$scope.dbRef.set({orgName : $scope.orgName, orgMail : $scope.orgMail},
		function(error)
		{
		    if(error)
			{
			    alert("Unable to register the organisation. Do try again after a while");
		        console.log("Adding organisation details failed");
			}
			else
			{
				var orgFoundString = "The organisation has been successfully registered.\n";
		        orgFoundString += "The code generated for the organisation is : " + $scope.code;
		        orgFoundString += "\nUse this code while Signing Up for an account.";
		        alert(orgFoundString);
			    console.log("The organisation " + $scope.orgName + " has been added.");
			}
		});
	}
	
	$scope.registerOrganisation = function()
	{		
		if ($scope.orgName && $scope.orgMail)
		{
		    console.log("Received the organisation details");
			
		    $scope.code =  CryptoJS.SHA1($scope.orgName).toString();
			var dbURL = 'https://pronirvahanadb.firebaseio.com/organisations/' + $scope.code;
			$scope.dbRef = new Firebase(dbURL);
			$scope.dbRef.once("value", function(snapshot) 
			{
				if(snapshot.exists())
				{
					console.log("The organisation " + snapshot.val().orgName + " is already registered");
					var orgFoundString = "The organisation is already registered.\n";
					orgFoundString += "The code generated for the organisation is : " + $scope.code;
					orgFoundString += "\nUse this code while Signing Up for an account.";
					alert(orgFoundString);
					found = true;
				}
				else
				{
					$scope.addOrganisationData();
				}
			});
			$location.path("/login");
		}
	}
	
	$scope.cancelRegistration = function()
	{
	    console.log("Organisation Registration action is cancelled by the user");
	    $scope.orgName = "";
		$scope.orgMail = "";
	    $location.path("/login");
	}
})
;
