// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'firebase'])

.run(function($ionicPlatform)
 {
    $ionicPlatform.ready(function()
	{
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard)
		{
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar)
		{
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.factory("Auth", ["$firebaseAuth",
  function($firebaseAuth) {
    var ref = new Firebase("https://pronirvahanadb.firebaseio.com/");
    return $firebaseAuth(ref);
  }
])

.factory("UserDataService", [function(){
	var userName = "undefined";
	var userOrg = "undefined";
	var userMail = "undefined";
	
	var factory = {};
	
	factory.getUserName = function(){
					return userName;
	}
	
	factory.getUserOrg = function(){
					return userOrg;
	}
	
	factory.getUserMail = function(){
					return userMail;
	}
	
	factory.setUserData = function(){
		dbRef = new Firebase("https://pronirvahanadb.firebaseio.com/");
		authData = dbRef.getAuth();
		var childRef = dbRef.child('users/' + authData.uid);
	
		console.log("The DB path to the current user: " + childRef.toString());
		
		childRef.once("value", function(snapshot)
		{
			if(snapshot.exists())
			{
				console.log("Retrieved the details of the user: " + authData.uid);
				var data = snapshot.val();
				userName = data.userName;
				console.log("The user name is : " + data.userName);
				userOrg = data.userOrgCode;
				console.log("The user org is : " + data.userOrgCode);
				userMail = data.userMail;
				console.log("The user mail is : " + data.userMail);
			}
		});
	}
	
	return factory;
}])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $locationProvider)
 {
    // Turn off caching for demo simplicity's sake
    $ionicConfigProvider.views.maxCache(0);
		
    $stateProvider.state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
    })
	
	.state('home', {
        url: '/home',
		templateUrl: 'templates/home.html',
        controller: 'HomeCtrl',
		resolve:
		{
			// controller will not be loaded until $requireAuth resolves
			// Auth refers to our $firebaseAuth wrapper in the example above
			"currentAuth": ["Auth", function(Auth)
			{
				// $requireAuth returns a promise so the resolve waits for it to complete
				// If the promise is rejected, it will throw a $stateChangeError
				return Auth.$requireAuth();
			}]
		}
    })

   .state('register', {
        url: '/register',
		templateUrl: 'templates/register.html',
        controller: 'RegisterCtrl'    
    })
	
	.state('signUp', {
        url: '/signUp',
        templateUrl: 'templates/signUp.html',
		controller: 'SignUpCtrl'
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
});
